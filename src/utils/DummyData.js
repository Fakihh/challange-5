export const loginData = {
    user: {
        role: "user",
        isEmailVerified: true,
        name: "Fakih",
        email: "ilhamfakih123@gmail.com",
        id: "624d3825bbe9dd7fb4f8810c",
    },
    tokens: {
        access: {
            token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjRkMzgyNWJiZTlkZDdmYjRmODgxMGMiLCJpYXQiOjE2NTA2NDc0NzEsImV4cCI6MTY1MDY0OTI3MSwidHlwZSI6ImFjY2VzcyJ9.NB067lMhyljMKLsEHUTH93PFP6SlOud-6nboIoj90xs",
            expires: "2022-04-22T17:41:11.709Z",
        },
        refresh: {
            token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjRkMzgyNWJiZTlkZDdmYjRmODgxMGMiLCJpYXQiOjE2NTA2NDc0NzEsImV4cCI6MTY1MzIzOTQ3MSwidHlwZSI6InJlZnJlc2gifQ.RniaP_O5WxSrCHYbPvSeSCAQC4U_2BMt9NKkf3LFUfU",
            expires: "2022-05-22T17:11:11.709Z",
        },
    },
};

export const registerData = {
    success: true,
    message: "Register succeed! Please try to login with email integrationtest@gmail.com",
    data: {
        role: "user",
        isEmailVerified: true,
        name: "integrationtest",
        email: "integrationtest@gmail.com",
        id: "626230d2dacc2e2fb47cb8fc",
    },
};

export const booksData = {
    results: [
        {
            title: "Harry Potter and the Order of the Phoenix",
            author: "J. K. Rowling",
            cover_image:
                "https://images-na.ssl-images-amazon.com/images/I/51SfTd37PaL._SX415_BO1,204,203,200_.jpg",
            id: "6231453513c01e6f8b566ece",
            publisher: "Scholastic",
            average_rating: "8",
            price: "292532",
        },
        {
            title: "Harry Potter and the Half-Blood Prince",
            author: "J. K. Rowling",
            cover_image:
                "https://images-na.ssl-images-amazon.com/images/I/51KV4CXARLL._SX342_BO1,204,203,200_.jpg",
            id: "6231453513c01e6f8b566ecf",
            publisher: "Scholastic",
            average_rating: "9",
            price: "287354",
        },
        {
            title: "Harry Potter and the Sorcerer's Stone",
            author: "J. K. Rowling",
            cover_image:
                "https://images-na.ssl-images-amazon.com/images/I/51HSkTKlauL._SX346_BO1,204,203,200_.jpg",
            id: "6231453513c01e6f8b566eca",
            publisher: "Scholastic",
            average_rating: "6",
            price: "71514",
        },
        {
            title: "Harry Potter and the Goblet of Fire",
            author: "J. K. Rowling",
            cover_image:
                "https://images-na.ssl-images-amazon.com/images/I/51OORp1XD1L._SX421_BO1,204,203,200_.jpg",
            id: "6231453513c01e6f8b566ecd",
            publisher: "Scholastic",
            average_rating: "4",
            price: "678107",
        },
        {
            title: "Harry Potter and the Chamber of Secrets",
            author: "J. K. Rowling",
            cover_image:
                "https://images-na.ssl-images-amazon.com/images/I/51jNORv6nQL._SX340_BO1,204,203,200_.jpg",
            id: "6231453513c01e6f8b566ecb",
            publisher: "Scholastic",
            average_rating: "10",
            price: "596609",
        },
        {
            title: "The Hobbit",
            author: "J. R. R. Tolkien",
            cover_image:
                "https://images-na.ssl-images-amazon.com/images/I/61VxEKq8B1L._SX365_BO1,204,203,200_.jpg",
            id: "6231453513c01e6f8b566ec9",
            publisher: "Houghton Mifflin Harcourt",
            average_rating: "4",
            price: "57424",
        },
        {
            title: "How to Be an Antiracist",
            author: "Ibram X. Kendi",
            cover_image:
                "https://images-na.ssl-images-amazon.com/images/I/51JM3rldZCL._SX329_BO1,204,203,200_.jpg",
            id: "6231453513c01e6f8b566ec7",
            publisher: "One World",
            average_rating: "7",
            price: "582825",
        },
        {
            title: "Voice of War",
            author: "Zack Argyle",
            cover_image: "https://images-na.ssl-images-amazon.com/images/I/41JodZ5Vl%2BL.jpg",
            id: "6231453513c01e6f8b566ec6",
            publisher: "Self Published",
            average_rating: "6",
            price: "177999",
        },
        {
            title: "The Lord of the Rings",
            author: "J. R. R. Tolkien",
            cover_image:
                "https://images-na.ssl-images-amazon.com/images/I/51r6XIPWmoL._SX331_BO1,204,203,200_.jpg",
            id: "6231453513c01e6f8b566ec8",
            publisher: "Houghton Mifflin Harcourt",
            average_rating: "5",
            price: "580848",
        },
        {
            title: "Harry Potter and the Prisoner of Azkaban",
            author: "J. K. Rowling",
            cover_image:
                "https://images-na.ssl-images-amazon.com/images/I/51IiQ4r35LL._SX345_BO1,204,203,200_.jpg",
            id: "6231453513c01e6f8b566ecc",
            publisher: "Scholastic",
            average_rating: "6",
            price: "259746",
        },
    ],
    page: 1,
    limit: 10,
    totalPages: 8,
    totalResults: 73,
};

export const bookDetailData = {
    title: "Harry Potter and the Order of the Phoenix",
    author: "J. K. Rowling",
    cover_image:
        "https://images-na.ssl-images-amazon.com/images/I/51SfTd37PaL._SX415_BO1,204,203,200_.jpg",
    id: "6231453513c01e6f8b566ece",
    page_count: "896",
    publisher: "Scholastic",
    synopsis:
        "In his fifth year at Hogwart's, Harry faces challenges at every turn, from the dark threat of He-Who-Must-Not-Be-Named and the unreliability of the government of the magical world to the rise of Ron Weasley as the keeper of the Gryffindor Quidditch Team. Along the way he learns about the strength of his friends, the fierceness of his enemies, and the meaning of sacrifice",
    total_sale: "644",
    average_rating: "8",
    price: "292532",
    stock_available: "51",
};
