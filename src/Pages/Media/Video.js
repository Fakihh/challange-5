import {View, Text} from 'react-native';
import React from 'react';
import Video from 'react-native-video';
import {VidLog} from '../Media/Video';
export {VidLog};

export default function video() {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>My Video Project</Text>
      <Video
        source={{
          uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
        }}
        style={{width: 300, height: 300, alignItems: 'center'}}
        fullscreen={true}
        controls={true}
        poster="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3
4/English_Cocker_Spaniel_4.jpg/800px-English_Cocker_Span
iel_4.jpg"
      />
    </View>
  );
}
