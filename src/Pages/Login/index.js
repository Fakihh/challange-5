import {StyleSheet, Text, View, TextInput, Button, Alert} from 'react-native';
import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {LoginSuccess} from '../../actions';
import Video from 'react-native-video';

const Login = ({navigation}) => {
  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const isLogin = useSelector(state => {
    console.log('its login?', state.appData.isLogin);
    return state.appData.isLogin;
  });

  function kirimData() {
    const data = {
      email: email,
      password: password,
    };
    console.log(data);
    dispatch(LoginSuccess(data));
    isLogin ? navigation.navigate('Home') : Alert.alert('Login Gagal');
  }

  return (
    <View style={styles.Container}>
      <View>
        <Video
          source={{
            uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
          }}
          style={{
            width: 300,
            height: 300,
            left: 60,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 60,
          }}
        />
      </View>

      <View style={{top: -60}}>
        <TextInput
          style={styles.text}
          placeholder="Email"
          value={email}
          onChangeText={text => setEmail(text)}
        />
        <TextInput
          style={styles.text}
          placeholder="Password"
          value={password}
          onChangeText={text => setPassword(text)}
          secureTextEntry={true}
        />
        <Button title="LOGIN" onPress={kirimData} />
        <Text style={styles.text2}>Belum punya akun?</Text>
        <Button
          title="Register"
          onPress={() => navigation.navigate('Register')}
        />
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  login: {
    marginTop: 45,
    marginHorizontal: 65,
    height: 200,
    width: 200,
    marginBottom: 50,
  },
  Container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  text: {
    borderWidth: 1,
    marginBottom: 10,
    borderColor: 'grey',
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#DEE6EF',
  },
  text2: {
    textAlign: 'center',
    marginVertical: 10,
    fontSize: 15,
    color: 'grey',
  },
});
