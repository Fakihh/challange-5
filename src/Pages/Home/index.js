import {
  StyleSheet,
  Text,
  View,
  Button,
  ScrollView,
  RefreshControl,
} from 'react-native';
import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {GetDataBook, isLoading, isRefresh} from '../../actions';
import { Recommended, PopularBook, Loading } from '../../Components';
import 'react-native-gesture-handler';


const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const userData = useSelector(state => {
    console.log('data user', state.appData.userData);
    return state.appData.userData;
  });

  const dataBook = useSelector(state => {
    console.log('Books: ', state.appData.dataBook);
    return state.appData.dataBook;
  });

  const isLoading = useSelector(state => state.appData.isLoading);
  const getRefresh = useSelector(state => state.appData.isRefresh);

  useEffect(() => {
    console.log(userData.tokens.access.token);
    dispatch(GetDataBook(userData.tokens.access.token));
  }, []);

  const Refresh = () => {
    dispatch(isRefresh(true));
    dispatch(GetDataBook(userData.tokens.access.token));
  };

  if (!isLoading) {
    return (
      <View style={styles.container}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={getRefresh}
              onRefresh={() => Refresh()}
            />
          }>
          <Text style={styles.textUp}>Wellcome, {userData.user.name}</Text>
          <View style={styles.recommend}>
            <Text style={styles.title}>Recommended</Text>
            <Recommended data={dataBook} />
          </View>
          <View style={styles.popular}>
            <Text style={styles.title}>Popular Book</Text>
            <PopularBook data={dataBook} />
          </View>
        </ScrollView>
      </View>
    );
  } else {
    return <Loading />;
  }
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#fff',
  },
  textUp: {
    color: '#323232',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 10,
  },
  title: {
    color: 'salmon',
    fontWeight: '600',
    fontSize: 13,
    flexWrap: 'wrap',
    marginBottom: 10,
  },
  popular: {
    marginVertical: 30,
  },
});
