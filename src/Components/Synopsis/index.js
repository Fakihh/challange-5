import {StyleSheet, Text, View, ScrollView} from 'react-native';
import React from 'react';

const Synopsis = data => {
  console.log(data);
  return (
    <View style={styles.container}>
      <Text style={styles.text}>DESCRIPTION</Text>
      <Text style={styles.text1}>{data.data.synopsis}</Text>
    </View>
  );
};

export default Synopsis;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 15,
  },
  text: {
    color: '#323232',
    fontWeight: 'bold',
    marginBottom: 10,
    fontSize: 25,
  },
  text1: {
    color: '#323232',
    fontSize: 14,
    textAlign: 'justify',
  },
});
