describe('App', () => {
  beforeAll(async () => {
    await device.launchApp({
      newInstance: true,
    });
  });

  afterAll(async () => {
    await device.terminateApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('can navigate between menu and order tabs', async () => {
    await expect(element(by.id('login-screen'))).toExist();
  });
});
