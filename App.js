import {View, Text, Image} from 'react-native';
import React, {useEffect} from 'react';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import Router from './src/Router';
import NetInfo from '@react-native-community/netinfo';
import {Store} from './src/store';
import {useState} from 'react';
import {NoInternet} from './src/Components';

function App() {
  const [isOffline, setOfflineStatus] = useState(false);

  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener(state => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });
    return () => removeNetInfoSubscription();
  }, []);

  return (
    <>
      {isOffline ? (
        <NoInternet />
      ) : (
        <Provider store={Store}>
          <NavigationContainer>
            <Router />
          </NavigationContainer>
        </Provider>
      )}
    </>
  );
}

export default App;
