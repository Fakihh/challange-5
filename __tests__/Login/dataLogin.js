export const dataLoginSuccess = {
    "user": {
        "role": "user",
        "isEmailVerified": true,
        "email": "ilhamfakih@gmail.com",
        "name": "fakih",
        "id": "624931a522536c7fc7ff8119"
    },
    "tokens": {
        "access": {
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjQ5MzFhNTIyNTM2YzdmYzdmZjgxMTkiLCJpYXQiOjE2NTA0NDU2ODYsImV4cCI6MTY1MDQ0NzQ4NiwidHlwZSI6ImFjY2VzcyJ9.YkQfNu2M5TqMiCK4lfomo0jpa4vrBB2q3_Ms7w5XgXo",
            "expires": "2022-04-20T09:38:06.329Z"
        },
        "refresh": {
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjQ5MzFhNTIyNTM2YzdmYzdmZjgxMTkiLCJpYXQiOjE2NTA0NDU2ODYsImV4cCI6MTY1MzAzNzY4NiwidHlwZSI6InJlZnJlc2gifQ.m69dYvQ0YpOEe8Bvco-EaEXnQZlcDJ9pp0oIiodZ3F0",
            "expires": "2022-05-20T09:08:06.329Z"
        }
    }
}

export const dataLoginFailed = {
    "code": 401,
    "message": "Incorrect email or password"
}