import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {dataLoginSuccess} from './dataLogin';

const LoggedIn = async data => {
  try {
    const response = await axios.post(
      'http://code.aldipee.com/api/v1/auth/login',
      data,
    );
    return response;
  } catch (err) {
    return console.log(err)
  }
};

describe('Login test', () => {
  let mockApi;
  beforeEach(() => {
    mockApi = new MockAdapter(axios);
  });

  afterEach(() => {
    mockApi.reset();
  });

  const loginSuccessBody = {
    email: 'ilhamfakih@gmail.com',
    password: 'fakih123',
  };
  const loginFailedBody = {
    email: 'ilhamfakih111@gmail.com',
    password: 'ilhami123',
  };

  it('Login success', async () => {
    mockApi
      .onPost('http://code.aldipee.com/api/v1/auth/login')
      .reply(200, dataLoginSuccess);

    const results = await LoggedIn(loginSuccessBody);
    expect(results.data).toEqual(dataLoginSuccess);
    expect(results.status).toEqual(200);
  });

  it('Login Failed', async () => {
    mockApi.onPost('http://code.aldipee.com/api/v1/auth/login').reply(400, []);
    const result = await LoggedIn(loginFailedBody);
    console.log(result);
    expect(result).toEqual();
  });
});
