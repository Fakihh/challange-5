import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { dataRegisterSuccess } from './dataRegister';

const Register = async (data) => {
  try {
    const res = await axios.post('http://code.aldipee.com/api/v1/auth/register', data);
    return res
  } catch (err) {
    return console.log(err)
  }
}

describe('Register test', () => {
  let mockApi 
  beforeEach(() => {
    mockApi = new MockAdapter(axios);
  })

  afterEach(() => {
    mockApi.reset();
  })
  
  const registerBody = {
      "email": "ilhamfakih@gmail.com",
      "password": "fakih123",
      "name": "fakihh"
  };

  it('Register success', async () => {
    mockApi.onPost('http://code.aldipee.com/api/v1/auth/register').reply(201, dataRegisterSuccess);

    const results = await Register(registerBody)
    expect(results.data).toEqual(dataRegisterSuccess);
    expect(results.status).toEqual(201);
  })

  it('Register Failed', async () => {
    mockApi.onPost('http://code.aldipee.com/api/v1/auth/register').reply(400, []);
    const result = await Register(registerBody);
    console.log(result);
    expect(result).toEqual();
  });
})