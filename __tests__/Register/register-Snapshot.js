import React from 'react'
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import renderer from 'react-test-renderer';

import {Register} from '../../src/Pages'

configure({ adapter: new Adapter(), disableLifecycleMethods: true });

jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));
jest.mock('react-native-push-notification', () => ({
  addEventListener: jest.fn(),
  requestPermissions: jest.fn(),
  then: jest.fn()
}));
jest.mock('react-native-share', () => ({
}));
jest.mock("react-native-video", () => "Video");
jest.mock("react-native-pdf", () => "Pdf");

const mockDispatch = jest.fn();


describe('Register Page', () => {
  it('should render properly', () => {
    const snap = renderer.create(<Register />);
    expect(snap).toMatchSnapshot();
  });
})