import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import renderer from 'react-test-renderer';

import {HomeScreen} from '../../src/Pages'

configure({ adapter: new Adapter(), disableLifecycleMethods: true });
const homeWrapper = shallow(<HomeScreen />)


jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));
jest.mock('react-native-push-notification', () => ({
  addEventListener: jest.fn(),
  requestPermissions: jest.fn(),
  then: jest.fn()
}));
jest.mock('react-native-share', () => ({
}));
jest.mock("react-native-video", () => "Video");
jest.mock("react-native-pdf", () => "Pdf");



const mockDispatch = jest.fn();

describe('Home Screen Snapshot', () => {
  it('should render correctly', () => {
    renderer.create(<HomeScreen />)
  })

  it('should render Home Screen module correctly', () => {
    expect(homeWrapper).toMatchSnapshot();
  })
})
